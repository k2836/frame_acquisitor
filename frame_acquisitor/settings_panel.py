import os
import tkinter as tk
from pathlib import Path
from tkinter import ttk
import tkmacosx


class SettingsPanel(tk.Frame):
    SAVE_DIR_ROOT = os.path.join(Path(os.path.realpath(__file__)).parent.parent, "acquired_datasets/")

    def __init__(self, master=None, cnf={}, **kw):
        self.camera = kw.pop("camera")
        super().__init__(master, cnf, **kw)
        self.dataset_root_dir = os.path.join(self.SAVE_DIR_ROOT, "pushup_pose")

        self.countdown_lbl = ttk.Label(self, text=" ", foreground="gray", font=("lucida", 76, "bold"))

        self.word_var = tk.StringVar()
        self.word_entry_lbl = tk.Label(self, text="Class:", font=("lucida", 24))
        self.word_entry = tk.Entry(self, textvariable=self.word_var, font=("lucida", 24), width=10)

        self.index = 0
        self.index_lbl = tk.Label(self, text="Video index:", font=("lucida", 24))
        self.index_val = tk.Label(self, text=self.index, font=("lucida", 24))

        btn_font = tk.font.Font(family='lucida', size=24)
        self.record_btn = tkmacosx.Button(self, text="Record", command=lambda: self._record_btn_action(), font=btn_font)
        self.next_btn = tkmacosx.Button(self, text="Next", command=lambda: self._next_btn_action(), font=btn_font, bg="#2686fb", fg="white")

    def grid(self, cnf={}, **kw):
        self.countdown_lbl.grid(row=0, column=0, columnspan=2, pady=(0, 30))

        self.word_entry_lbl.grid(row=1, column=0, sticky="w", padx=(20, 0), pady=(0, 20))
        self.word_entry.grid(row=1, column=1, padx=(0, 20), pady=(0, 20))

        self.index_lbl.grid(row=2, column=0, sticky="w", padx=(20, 0), pady=(0, 20))
        self.index_val.grid(row=2, column=1, sticky="w", padx=(0, 20), pady=(0, 20))

        self.record_btn.grid(row=3, column=0, padx=(20, 0))
        self.next_btn.grid(row=3, column=1, padx=(0, 20))
        self._manage_ui()
        super().grid_configure(cnf, **kw)

    @property
    def save_dir(self):
        return os.path.join(
            self.SAVE_DIR_ROOT,
            "pushup_pose",
            self.word_var.get()
        )

    @property
    def save_path(self):
        return os.path.join(self.save_dir, str(self.index) + self.camera.codec_suffix)

    def _count_down(self, i=3):
        if i > 0:
            self.countdown_lbl.config(text=i)
            self.master.after(700, self._count_down, i - 1)
        else:
            self.camera.record_segment(self.save_path)
            self.countdown_lbl.config(text=" ")

    def _manage_ui(self):
        if not self.word_var.get():
            self.index_val.config(text="Fill in the class")
            self.record_btn.grid_forget()
        # elif Path(self.save_dir).exists():
        #     prefix = lambda filename: filename[0].split(".")[0]
        #     self.index = max([int(prefix(f)) for _, _, f in os.walk(self.save_dir) if prefix(f).isnumeric()]) + 1
        #     self.index_val.config(text=self.index)
        elif not Path(self.save_dir).exists():
            self.index = 0
            self.index_val.config(text=self.index)

        if self.word_var.get():
            self.record_btn.grid(row=3, column=0, padx=(20, 0))

        if Path(self.save_path).exists():
            self.next_btn.grid(row=3, column=1, padx=(0, 20))
            self.record_btn.config(text="Correct", state="normal", bg="#f5f5f5", fg="black")  # normal
        else:
            self.next_btn.grid_forget()
            self.record_btn.config(text="Record", bg="#2686fb", fg="white")  # active

        self.after(100, self._manage_ui)

    def _next_btn_action(self):
        self.index += 1
        self.index_val.config(text=self.index)

    def _record_btn_action(self):
        parent_dir = Path(self.save_path).parent
        if not parent_dir.exists():
            os.mkdir(parent_dir)
        self._count_down(3)
        # self.camera.record_segment(self.save_path)
