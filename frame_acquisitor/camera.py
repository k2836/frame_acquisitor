import threading
import time
from datetime import datetime

import cv2.cv2 as cv2


class ThreadCamera:
    def __init__(self):
        self.capture_frame = 60
        self.fps_limit = 30
        self.fps = 0
        self.capture = cv2.VideoCapture(0)
        self.capture.set(cv2.CAP_PROP_BUFFERSIZE, 2)
        self.thread_flag = False
        self.record_flag = False
        self.save_dir = None

        self.width = int(self.capture.get(cv2.CAP_PROP_FRAME_WIDTH) + 0.3)
        self.height = int(self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT) + 0.3)

        self.codec_suffix = ".jpg"
        self.fourcc = cv2.VideoWriter_fourcc(*'XVID')
        self.out = None

    def start_recording(self):
        thread = threading.Thread(target=self._update_frame, args=())
        thread.daemon = True
        self.thread_flag = True
        thread.start()
        self._load_frame()

    def record_segment(self, save_dir):
        self.record_flag = True
        self.save_dir = save_dir
        # self.out = cv2.VideoWriter(save_dir, self.fourcc, 30.0, (self.width, self.height))

    def stop_recording(self):
        self.thread_flag = False
        time.sleep(1)
        self.capture.release()

    def get_frame(self):
        frame = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)
        return frame

    def _update_frame(self):
        segment_length = 0
        while self.thread_flag and self.capture.isOpened():
            ret, frame = self.capture.read()
            if ret:
                frame = cv2.flip(frame, 1)
                self.frame = frame
                if self.record_flag:
                    segment_length += 1
                if segment_length == self.capture_frame:
                    cv2.imwrite(self.save_dir, frame)
                    self.record_flag = False
                    segment_length = 0

        # segment_length = 0
        # while self.thread_flag and self.capture.isOpened():
        #     ret, frame = self.capture.read()
        #     if ret:
        #         frame = cv2.flip(frame, 1)
        #         self.frame = frame
        #         if self.out and segment_length == self.capture_frame:
        #             self.out.write(frame)
        #             self.out.release()
        #             self.out = None
        #             segment_length = 0
        #         segment_length += 1

    def _load_frame(self):
        """ Wait for the thread to load first frame """
        while True:
            time.sleep(0.01)
            if hasattr(self, "frame"):
                return

    def __enter__(self):
        self.start_recording()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop_recording()

    def __del__(self):
        if self.capture.isOpened():
            self.stop_recording()
