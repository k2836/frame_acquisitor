import tkinter as tk

from frame_acquisitor.settings_panel import SettingsPanel
from frame_acquisitor.video_window import VideoWindow


class MainLayout(tk.Frame):

    def grid(self, cnf={}, **kw):
        video_window = VideoWindow(self.master)
        video_window.grid(row=0, column=0)
        SettingsPanel(self.master, camera=video_window.camera).grid(row=0, column=1)
        super().grid(cnf, **kw)
