import tkinter as tk
import cv2

from frame_acquisitor.main_frame import MainLayout


def main():
    root = tk.Tk()
    root.title("")
    root.resizable(False, False)
    main_frame = MainLayout(root)
    main_frame.grid(row=0, column=0)
    root.eval('tk::PlaceWindow . center')
    main_frame.master.mainloop()


if __name__ == "__main__":
    main()
