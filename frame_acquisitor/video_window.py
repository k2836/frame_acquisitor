import tkinter as tk
from tkinter import ttk

from PIL import ImageTk, Image

from frame_acquisitor.camera import ThreadCamera


class VideoWindow(tk.Frame):

    def __init__(self, master=None, cnf={}, **kw):
        super().__init__(master, cnf, **kw)
        self.camera = ThreadCamera()

    def grid(self, cnf={}, **kw):
        self.camera.start_recording()

        video_window = ttk.Label(self)
        video_window.grid(row=0, column=0)

        tk_img = ImageTk.PhotoImage(image=Image.fromarray(self.camera.get_frame()))
        video_window._image_cache = tk_img  # Alternative: video_window.imgtk = tk_img
        video_window.configure(image=tk_img)
        self.render_scene(video_window)
        super().grid_configure(cnf, **kw)

    def render_scene(self, video_window):
        # Video window
        if self.camera.record_flag:
            tk_img = ImageTk.PhotoImage(image=Image.fromarray(self.camera.get_frame()))
            video_window._image_cache = tk_img  # Alternative: video_window.imgtk = tk_img
            video_window.configure(image=tk_img)
        video_window.after(10, self.render_scene, video_window)

    def close(self):
        self.camera.stop_recording()
